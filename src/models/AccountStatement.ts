import { MintosOperation } from "./MintosOperation";

const interestName = ["ODSETKI", "INTEREST"];
const feeExchangeName = ["EXCHANGE"];
const secondaryMarketTrancactionDiscount = [
  "transakcja na rynku wtórnym",
  "secondary market transaction"
];

export class AccountStatement {
  public currency: string;
  public operations: Array<MintosOperation>;

  constructor() {
    this.operations = new Array<MintosOperation>();
  }

  public getHeader(): string[] {
    return [
      "id",
      "transaction_id",
      "operation_date",
      "details",
      "mintos_turnovers",
      "user_turnovers",
      "user_currency_date",
      "exchange_rate"
    ];
  }

  public getData(): any {
    let dataArray = new Array();

    if (
      this.operations.length > 0 &&
      this.operations[0].currencyRate !== undefined
    ) {
      this.operations.forEach(function(value, i) {
        dataArray.push({
          id: i + 1,
          transaction_id: value.getTransactionId(),
          operation_date: value.getOperationDate(),
          details: value.getShortDescript(),
          mintos_turnovers: value.getMintosTurnoverString(),
          user_turnovers: value.getUserTurnovers(),
          user_currency_date: value.getCurrencyDate(),
          exchange_rate: value.getCurrencyRate()
        });
      });
    }

    return dataArray;
  }

  public getCurrency(): string {
    return this.currency;
  }

  public getInterestOperationNumber(): number {
    return this.operations.filter(q => this.includsAny(q.details.toUpperCase(), interestName))
      .length;
  }

  //Pobiera odsetki naliczone w walucie użytkownika
  public getUserInterestSum(): number {
    let interestSum = 0.0;

    if (this.operations.length !== 0) {
      this.operations.forEach(element => {
        if (this.includsAny(element.details.toUpperCase(), interestName)) {
          interestSum += element.turnover * element.currencyRate;
        }
      });
    }

    return interestSum;
  }

  /// Pobiera odsetki naliczone przez mintos w walucie, w której jest zestawienie
  public getMintosInterestSum(): string {
    let interestSumString = "0";

    if (this.operations.length !== 0) {
      let interestSum = 0.0;

      this.operations.forEach(element => {
        if (this.includsAny(element.details.toUpperCase(), interestName)) {
          interestSum += element.turnover;
        }
      });

      interestSumString = interestSum.toFixed(4);
    }

    return interestSumString;
  }

  //Pobiera odsetki naliczone w walucie użytkownika
  public getUserExchangeFee(): number {
    let interestSum = 0.0;

    if (this.operations.length !== 0) {
      this.operations.forEach(element => {
        if (this.includsAny(element.details.toUpperCase(), feeExchangeName)) {
          interestSum += element.turnover * element.currencyRate;
        }
      });
    }

    return interestSum;
  }

  //Pobiera odsetki naliczone w walucie użytkownika
  public getSecondaryMarketDiscount(): number {
    let interestSum = 0.0;

    if (this.operations.length !== 0) {
      this.operations.forEach(element => {
        if (
          this.includsAny(element.details, secondaryMarketTrancactionDiscount)
        ) {
          interestSum += element.turnover * element.currencyRate;
        }
      });
    }

    return interestSum;
  }

  //#region Metody prywatne

  // Sprawdź czy string zawiera jakikolwiek ciąg znaków z podanych
  private includsAny(str: string, items: Array<string>) {
    for (let i in items) {
      let item = items[i];
      if (str.includes(item)) {
        return true;
      }
    }
    return false;
  }

  //#endregion
}
