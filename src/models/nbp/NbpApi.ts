import moment from "moment";
import Axios from "axios";
import { NbpDataModel } from "./NbpDataModel";

const tableA = ["CZK", "DKK", "EUR", "GBP", "RUB", "SEK", "USD", "MXN", "RON"];
const tableB = ["GEL", "KZT"];

export class NbpApi {
  private dateFrom: Date;
  private dateTo: Date;
  private currency: string;
  private urls: Array<string>;

  constructor(dateFrom: Date, dateTo: Date, currency: string) {
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.currency = currency.trim();
    this.urls = new Array<string>();

    // przygotuj adresy API
    this.getApiUrls();
  }

  private getApiUrls() {
    let table;
    let days;
    let dateTo;
    let dateFrom;

    if (tableA.includes(this.currency)) {
      table = "A";
    } else if (tableB.includes(this.currency)) {
      table = "B";
    } else {
      throw new Error("Non specific currency table!");
    }

    // policz różnice w dniach podanych dat
    days = moment(this.dateTo).diff(moment(this.dateFrom), "days") + 1;
    dateFrom = moment(this.dateTo);
    dateTo = moment(this.dateTo);

    try {
      // API NPB zwraca max 367 wartości kursu walut - dla dłuższego przedzialu potrzebne jest kilka zapytań
      let i;
      for (i = 1; i <= Math.ceil(days / 360); i++) {
        this.urls.push(
          "https://api.nbp.pl/api/exchangerates/rates/" +
            table +
            "/" +
            this.currency +
            "/" +
            dateFrom.add(-360, "days").format("YYYY-MM-DD") +
            "/" +
            dateTo.add(i === 1 ? 0 : -360, "days").format("YYYY-MM-DD") +
            "/?format=json"
        );
      }

      console.log(this.urls);
    } catch {
      throw new Error(
        "Problem with create apiUrls array. DateFrom: " +
          dateFrom +
          "; DateTo: " +
          dateTo
      );
    }
  }

  public async getCurrencyRateFromNbp(): Promise<any> {
    let promisesArray: Array<Promise<any>> = [];

    this.urls.forEach(url => {
      promisesArray.push(
        new Promise<NbpDataModel>(resolve => {
          console.log(url);
          Axios.get<NbpDataModel>(url).then(response => {
            resolve(response.data);
          });
        })
      );
    });

    return Promise.all(promisesArray);
  }
}
