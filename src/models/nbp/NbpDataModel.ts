import { CurrencyRate } from "./CurrencyRate";
import * as moment from "moment";

export class NbpDataModel {
  public code: string;
  public currency: string;
  public rates: Array<CurrencyRate>;
  public ratesDict: Map<string, CurrencyRate>;

  constructor() {
    this.rates = new Array<CurrencyRate>();
    this.ratesDict = new Map();
  }

  // Pobierz kurs waluty dla konkretnej daty
  public getMidForDate(date: moment.Moment): any {
    let findingRate = this.ratesDict.get(date.format("YYYY-MM-DD"));
    return findingRate;
  }

  // Stwórz słownik w postacie {Data, kurs}
  public createDict(): void {
    if (this.rates.length > 0) {
      this.rates.forEach(element => {
        this.ratesDict.set(String(element.effectiveDate), element);
      });
    }
  }
}
