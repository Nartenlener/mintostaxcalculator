import moment from "moment";

// Klasa modelująca rekord na dokumencie XSLX wyciągu z konta
export class MintosOperation {
  public transactionId: string;
  public transactionDate: Date;
  public details: string;
  public turnover: number;
  public balance: number;
  public currency: string;
  public currencyRate: number;
  public currencyDate: Date;

  constructor(
    transactionId: string,
    transactionDate: Date,
    details: string,
    turnover: number,
    balance: number,
    currency: string
  ) {
    this.transactionId = transactionId;
    this.transactionDate = transactionDate;
    this.details = details;
    this.turnover = turnover;
    this.balance = balance;
    this.currency = currency;
  }

  public getOperationDate(): string {
    return moment(this.transactionDate).format("YYYY-MM-DD hh:mm:ss");
  }

  public getMintosTurnoverString(): string {
    return this.turnover.toFixed(6) + " " + this.currency;
  }

  public getTransactionId(): string {
    return this.transactionId;
  }

  public getShortDescript(): string {
    let shortString = "";

    if (this.details.length < 40) {
      shortString = this.details;
    } else {
      shortString = this.details.substring(0, 34) + "...";
    }
    return shortString;
  }

  public addCurrencyData(currendyDate: Date, currencyRate: number) {
    this.currencyDate = currendyDate;
    this.currencyRate = currencyRate;
  }

  public getCurrencyRate(): string {
    return this.currencyRate.toFixed(4);
  }

  public getUserTurnovers(): string {
    return (this.currencyRate * this.turnover).toFixed(4) + " PLN";
  }

  public getCurrencyDate(): string {
    return moment(this.currencyDate).format("YYYY-MM-DD");
  }
}
