import Vue from "vue";
import App from "./App.vue";
import VueAnalytics from "vue-analytics";

Vue.config.productionTip = false;
const isProd = process.env.NODE_ENV === "production";

Vue.use(VueAnalytics, {
  id: "UA-133577691-1",
  debug: {
    enable: !isProd,
    sendHitTask: isProd
  }
});

new Vue({
  render: h => h(App)
}).$mount("#app");
